//
//  WorkoutController.swift
//  phDraft
//
//  Created by Daniel O'Leary on 5/26/20.
//  Copyright © 2020 Impulse Coupled Development. All rights reserved.
//

import Combine
import HealthKit

class WorkoutController: NSObject, ObservableObject {
	@Published var heartRate = 0
	
	let healthStore = HKHealthStore()
	
	
	override init() {
		super.init()
		
		// The quantity type to write to the health store.
		let typesToShare: Set = [HKQuantityType.workoutType()]
		
		let typesToRead: Set = [
			HKQuantityType.quantityType(forIdentifier: .heartRate)!,
			HKQuantityType.quantityType(forIdentifier: .activeEnergyBurned)!,
			HKQuantityType.quantityType(forIdentifier: .distanceWalkingRunning)!
		]
		
		// Request authorization for those quantity types.
		// MARK: Reference Info.plist to view Privacy permissions needed to request authorization from the user.
		healthStore.requestAuthorization(toShare: typesToShare, read: typesToRead) { (success, error) in
			// Handle errors.
			
		}
		
	}
	
//	func getHeathStore() {
//
//		let components = DateComponents(calendar: .current, year: 2020, month: 1)
//		let query = HKQuery.
//
//		healthStore.execute(query)
//	}
	
}
