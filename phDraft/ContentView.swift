//
//  ContentView.swift
//  phDraft
//
//  Created by Daniel O'Leary on 5/23/20.
//  Copyright © 2020 Impulse Coupled Development. All rights reserved.
//

import SwiftUI

struct ContentView: View {
	@ObservedObject var viewModel: ViewModel
	@ObservedObject var workoutController = WorkoutController()
	@ObservedObject var watchConnectivity: ConnectivityProvider
	
	
	var body: some View {
		ZStack {
			VideoPlayer()
			
			VStack {
				HStack {
					Text("Heartrate: \(watchConnectivity.heartRate)")
						.foregroundColor(.white)
					Image(systemName: "heart")
						.foregroundColor(.red)
					Spacer()
					Text("Calories: \(watchConnectivity.calories)")
						.foregroundColor(.white)
					Image(systemName: "flame")
						.foregroundColor(.red)
				}
				.font(.title)
				Spacer()
		//			Button(action: {
		//				// launch workout on watch
		//				print("Does nothing")
		//			}, label: {
		//				Image(systemName: "play.rectangle")
		//					.font(.largeTitle)
		//			})
			}
			
		}
		
	}
}





struct ContentView_Previews: PreviewProvider {
	static let viewModel = ViewModel(connectivityProvider: ConnectivityProvider())
	static var previews: some View {
		ContentView(viewModel: viewModel, watchConnectivity: ConnectivityProvider())
	}
}
