//
//  WCManager.swift
//  WatchCommunication
//
//  Created by Daniel O'Leary on 5/27/20.
//  Copyright © 2020 Impulse Coupled Development. All rights reserved.
//

import SwiftUI
import WatchConnectivity


class ConnectivityProvider: NSObject, WCSessionDelegate, ObservableObject {
	@Published var heartRate = " "
	@Published var calories = " "
	
    private let session: WCSession

    init(session: WCSession = .default) {
        self.session = session
        super.init()
        self.session.delegate = self
		self.session.activate()
    }

    func send(message: [String:Any]) -> Void {
		if session.isReachable {
			// From `.isReachAble` docs: A paired and active Apple Watch is in range, the corresponding WatchKit extension is running, and the WatchKit extension’s isReachable property is true.
			session.sendMessage(message, replyHandler: nil) { (error) in
				print(error.localizedDescription)
			}
		} else {
			print("Session paired? \(session.isPaired)")
			print("Watch session might be paired but not reachable.")
		}
    }

    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
        // code
		print("Session activated with state \(activationState.rawValue)")
		print("Session found apple watch app installed \(session.isWatchAppInstalled)")
    }
	
	func sessionDidBecomeInactive(_ session: WCSession) {
		// code
		print("Session did beccome inactive \(session.activationState)")
	}
	
	func sessionDidDeactivate(_ session: WCSession) {
		// code
		print("Session did deactivate \(session.activationState)")
	}
	
	func session(_ session: WCSession, didReceiveMessage message: [String : Any]) {
        print("\(#function): \(message)")
		if let heartrate = message["bpm"] as? String {
			// Push to main thread to update ContentView
			DispatchQueue.main.async {
				self.heartRate = heartrate
			}
		}
		
		if let calories = message["cal"] as? String {
			DispatchQueue.main.async {
				self.calories = calories
			}
		}
		
	}
	
	
}


class ViewModel: ObservableObject {
	// MARK: There is additional setup inside the SceneDelegate needed to tie everything together in SwiftUI.
	
	let connectivityProvider: ConnectivityProvider
	var textFieldValue: String = ""
	
	init(connectivityProvider: ConnectivityProvider) {
		self.connectivityProvider = connectivityProvider
	}
	
	func sendMessage() -> Void {
		let txt = textFieldValue
		let message = ["message":txt]
		connectivityProvider.send(message: message)
	}
	

}
