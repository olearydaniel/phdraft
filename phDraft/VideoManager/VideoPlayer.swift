//
//  VideoPlayer.swift
//  phStream
//
//  Created by Daniel O'Leary on 6/2/20.
//  Copyright © 2020 Impulse Coupled Development. All rights reserved.
//

import AVKit
import SwiftUI


struct VideoPlayer: UIViewControllerRepresentable {
	
	func makeUIViewController(context: UIViewControllerRepresentableContext<VideoPlayer>) -> AVPlayerViewController {
		let controller = AVPlayerViewController()
		
		if let url = URL(string: "https://videodelivery.net/a63dcf815facee1dc54b703a81a5cb18/manifest/video.m3u8") {
			let player = AVPlayer(url: url)
			controller.player = player
		}
		
		return controller
	}
	
	func updateUIViewController(_ uiViewController: AVPlayerViewController, context: Context) {
		// Required but unused.
	}
	
}
