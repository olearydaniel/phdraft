//
//  WCManagerWatchOS.swift
//  WatchCommunication WatchKit Extension
//
//  Created by Daniel O'Leary on 5/27/20.
//  Copyright © 2020 Impulse Coupled Development. All rights reserved.
//

import SwiftUI
import WatchConnectivity


class ConnectivityProvider: NSObject, WCSessionDelegate, ObservableObject {
	@Published var text = ""
    private let session: WCSession

    init(session: WCSession = .default) {
        self.session = session
        super.init()
        self.session.delegate = self
		self.session.activate()
    }

    func send(message: [String:Any]) -> Void {
		if session.isReachable {
            print("\(#function) isReachable")
			// From `.isReachAble` docs: A paired and active Apple Watch is in range, the corresponding WatchKit extension is running, and the WatchKit extension’s isReachable property is true.
			session.sendMessage(message, replyHandler: nil) { (error) in
				print("Error in watchOS send message: \(error.localizedDescription)")
			}
		} else {
			print("\(#function): iOS session not reachable.")
		}
    }

    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
		print("Session activated with state \(activationState)")
		print("Session found iOS App installed \(session.isCompanionAppInstalled)")
		print("iPhone is locked \(session.iOSDeviceNeedsUnlockAfterRebootForReachability)")
    }
	
	func session(_ session: WCSession, didReceiveMessage message: [String : Any]) {
        print("\(#function): \(message)")
		if let text = message["message"] as? String {
			// Push to main thread to update ContentView
			DispatchQueue.main.async {
				self.text = text
			}
		}
	}
	
}


class WatchViewModel: ObservableObject {
	// MARK: There is additional setup inside the HostingController needed to tie everything together in SwiftUI.
	let connectivityProvider: ConnectivityProvider
	
	init(connectivityProvider: ConnectivityProvider) {
		self.connectivityProvider = connectivityProvider
	}
	
	func sendMessage() -> Void {
		let txt = "Hello from watchOS!"
		let message = ["message":txt]
		
		connectivityProvider.send(message: message)
	}

}
