//
//  WatchWorkoutController.swift
//  phDraft WatchKit Extension
//
//  Created by Daniel O'Leary on 5/26/20.
//  Copyright © 2020 Impulse Coupled Development. All rights reserved.
//

import SwiftUI
import Foundation
import HealthKit
import WatchKit

class WatchWorkoutController: NSObject, ObservableObject, HKWorkoutSessionDelegate, HKLiveWorkoutBuilderDelegate {
	
	//@Published var timer: WKInterfaceTimer!
	// No means of using a WKInterfaceTimer in SwiftUI as far as I can tell - 5/26/2020
	@Published var heartRate = "💗"
	@Published var energyBurned = ""
	@Published var distance = ""
	
	let healthStore = HKHealthStore()
	
	var session: HKWorkoutSession!
	var builder: HKLiveWorkoutBuilder!
	
	let connect = ConnectivityProvider()
	
	// WKInterfaceLabels
	weak var activeCaloriesLabel: WKInterfaceLabel!
	weak var heartRateLabel: WKInterfaceLabel!
    weak var distanceLabel: WKInterfaceLabel!
	
	func setupWorkoutSession() {
		let typesToShare: Set = [HKQuantityType.workoutType()]
		
		let typesToRead: Set = [
			HKQuantityType.quantityType(forIdentifier: .heartRate)!,
			HKQuantityType.quantityType(forIdentifier: .activeEnergyBurned)!,
			HKQuantityType.quantityType(forIdentifier: .distanceWalkingRunning)!
		]
		// Request authorization for those quantity types.
		// MARK: Reference Info.plist to view Privacy permissions needed to request authorization from the user.
		healthStore.requestAuthorization(toShare: typesToShare, read: typesToRead) { (success, error) in
			// Handle errors.
			if success {
				self.beginWorkout()
			}
		}
	}
	
	func beginWorkout() {
		/* From Apple: "You must make this request in both the WatchKit extension
		and in the companion iOS app, because watchOS will ask the user to give
		authorization on the companion iPhone."
		*/
		let configuration = HKWorkoutConfiguration()
		configuration.activityType = .running
		configuration.locationType = .outdoor
				
		do {
			session = try HKWorkoutSession(healthStore: healthStore, configuration: configuration)
			builder = session.associatedWorkoutBuilder()
		} catch {
			// handle errors
		}
		
		// Setup session and builder.
		session.delegate = self
		builder.delegate = self
		
		// Set the workout builder's data source.
		builder.dataSource = HKLiveWorkoutDataSource(healthStore: healthStore, workoutConfiguration: configuration)
		
		/// - Tag: StartSession
		session.startActivity(with: Date())
		builder.beginCollection(withStart: Date()) { (success, error) in
			self.setDurationTimerDate(.running)
		}
	}
	
	func endWorkout() {
		session.end()
		builder.endCollection(withEnd: Date()) { (success, error) in
			self.builder.finishWorkout { (workout, error) in
				// Dispatch to main, because we are updating the interface.
				DispatchQueue.main.async() {
					// Not used at this point 5.29.2020
//					self.presentationMode.wrappedValue.dismiss()
				}
			}
		}
	}
	
	// Track elapsed time.
	func workoutBuilderDidCollectEvent(_ workoutBuilder: HKLiveWorkoutBuilder) {
		// Retreive the workout event.
		guard let workoutEventType = workoutBuilder.workoutEvents.last?.type else { return }
		
		// Update the timer based on the event received.
		switch workoutEventType {
			case .pause: // The user paused the workout.
				setDurationTimerDate(.paused)
			case .resume: // The user resumed the workout.
				setDurationTimerDate(.running)
			default:
				return
		}
	}
	
	
	func setDurationTimerDate(_ sessionState: HKWorkoutSessionState) {
//		// Unused but was part of original codebase for updating a timer label.
//
//		// Obtain the elapsed time from the workout builder.
//		let timerDate = Date(timeInterval: -self.builder.elapsedTime, since: Date())
//
//		// Dispatch to main, because we are updating the interface.
//		DispatchQueue.main.async {
//			self.timer.setDate(timerDate)
//
//		}
//
//		// Dispatch to main, because we are updating the interface.
//		DispatchQueue.main.async {
//			// Update the timer based on the state we are in.
//			sessionState == .running ? self.timer.start() : self.timer.stop()
//		}
	}
	
	// MARK: - HKLiveWorkoutBuilderDelegate
	func workoutBuilder(_ workoutBuilder: HKLiveWorkoutBuilder, didCollectDataOf collectedTypes: Set<HKSampleType>) {
	        for type in collectedTypes {
	            guard let quantityType = type as? HKQuantityType else {
	                return
	            }
	
	            let statistics = workoutBuilder.statistics(for: quantityType)
				let label = labelForQuantityType(quantityType)
	
	            updateLabel(label, withStatistics: statistics)
	        }
	}
	
	/// Update the WKInterfaceLabels with new data.
    func updateLabel(_ label: WKInterfaceLabel?, withStatistics statistics: HKStatistics?) {
        // Make sure we got non `nil` parameters.
//        guard let label = label, // Unused
		guard let statistics = statistics else { return }
        
        // Dispatch to main, because we are updating the interface.
        DispatchQueue.main.async {
            switch statistics.quantityType {
            case HKQuantityType.quantityType(forIdentifier: .heartRate):
                /// - Tag: SetLabel
                let heartRateUnit = HKUnit.count().unitDivided(by: HKUnit.minute())
                let value = statistics.mostRecentQuantity()?.doubleValue(for: heartRateUnit)
                let roundedValue = Double( round( 1 * value! ) / 1 )
				self.heartRate = "\(roundedValue) BPM"
				self.connect.send(message: ["bpm":"\(roundedValue) BPM"])
            case HKQuantityType.quantityType(forIdentifier: .activeEnergyBurned):
                let energyUnit = HKUnit.kilocalorie()
                let value = statistics.sumQuantity()?.doubleValue(for: energyUnit)
                let roundedValue = Double( round( 1 * value! ) / 1 )
				self.energyBurned = "\(roundedValue)"
				self.connect.send(message: ["cal" : "\(roundedValue) Cal"])
                return
            case HKQuantityType.quantityType(forIdentifier: .distanceWalkingRunning):
                let meterUnit = HKUnit.meter()
                let value = statistics.sumQuantity()?.doubleValue(for: meterUnit)
                let roundedValue = Double( round( 1 * value! ) / 1 )
				self.distance = "\(roundedValue) m"
                return
			default:
                return
            }
        }
    }
	
	func labelForQuantityType(_ type: HKQuantityType) -> WKInterfaceLabel? {
        switch type {
        case HKQuantityType.quantityType(forIdentifier: .heartRate):
            return heartRateLabel
        case HKQuantityType.quantityType(forIdentifier: .activeEnergyBurned):
            return activeCaloriesLabel
        case HKQuantityType.quantityType(forIdentifier: .distanceWalkingRunning):
            return distanceLabel
        default:
            return nil
        }
    }
	
	
	func workoutSession(_ workoutSession: HKWorkoutSession, didChangeTo toState: HKWorkoutSessionState, from fromState: HKWorkoutSessionState, date: Date) {
		// I belive this is for changing the type of workout in the middle of another workout. Docs are unclear...
		// https://developer.apple.com/documentation/healthkit/hkworkoutsessiondelegate/1627958-workoutsession
	}
	
	func workoutSession(_ workoutSession: HKWorkoutSession, didGenerate event: HKWorkoutEvent) {
		// MARK: Elevation infomation can be found here:
		// https://developer.apple.com/documentation/healthkit/hkmetadatakeyelevationdescended
		
		// Commented out to gather any event.type
//		if event.type == .segment {
			print("\(#function) called!")
			if let ascent = event.metadata?[HKMetadataKeyElevationAscended] as? HKQuantity,
				ascent.is(compatibleWith: .meter()) {
				let meters = ascent.doubleValue(for: .meter())
				print("Climb: \(meters)m")
			} else {
				print("No up-ness 4 U")
			}
			
			if let descent = event.metadata?[HKMetadataKeyElevationDescended] as? HKQuantity,
				descent.is(compatibleWith: .meter()) {
				let meters = descent.doubleValue(for: .meter())
				print("Descent: \(meters)m")
			} else {
				print("No down-y down here.")
			}
//		}
	}
	
	func workoutSession(_ workoutSession: HKWorkoutSession, didFailWithError error: Error) {
		// no errors handled
	}
	
	
	
}
