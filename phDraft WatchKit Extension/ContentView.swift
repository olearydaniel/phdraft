//
//  ContentView.swift
//  phDraft WatchKit Extension
//
//  Created by Daniel O'Leary on 5/23/20.
//  Copyright © 2020 Impulse Coupled Development. All rights reserved.
//

import SwiftUI

struct ContentView: View {
	@ObservedObject var workoutController = WatchWorkoutController()
	
    var body: some View {
		VStack(alignment: .leading) {
			Text("Calories: \(workoutController.energyBurned)")
			Text("Heart rate: \(workoutController.heartRate)")
			Text("Distance: \(workoutController.distance)")
//			Text("Timer: \(workoutController.timer)")
			Button(action: {
				self.workoutController.setupWorkoutSession()
			}, label: {
				Text("Begin Workout")
					.foregroundColor(.blue)
			})
			Button(action: {
				self.workoutController.endWorkout()
			}, label: {
				Text("Stop")
					.foregroundColor(.red)
			})
		}
    }
	
	
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
