//
//  HostingController.swift
//  phDraft WatchKit Extension
//
//  Created by Daniel O'Leary on 5/23/20.
//  Copyright © 2020 Impulse Coupled Development. All rights reserved.
//

import WatchKit
import Foundation
import SwiftUI

class HostingController: WKHostingController<ContentView> {
    override var body: ContentView {
        return ContentView()
    }
}
