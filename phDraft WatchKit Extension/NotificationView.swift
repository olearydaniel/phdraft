//
//  NotificationView.swift
//  phDraft WatchKit Extension
//
//  Created by Daniel O'Leary on 5/23/20.
//  Copyright © 2020 Impulse Coupled Development. All rights reserved.
//

import SwiftUI

struct NotificationView: View {
    var body: some View {
        Text("Hello, World!")
    }
}

struct NotificationView_Previews: PreviewProvider {
    static var previews: some View {
        NotificationView()
    }
}
